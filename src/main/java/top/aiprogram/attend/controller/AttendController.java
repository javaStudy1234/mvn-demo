package top.aiprogram.attend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.aiprogram.attend.entity.Attend;
import top.aiprogram.attend.service.AttendService;
import top.aiprogram.attend.vo.QueryCondition;
import top.aiprogram.common.page.PageQueryBean;
import top.aiprogram.user.entity.User;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("attend")
public class AttendController {

    @Autowired
    private AttendService attendService;

    @RequestMapping
    public String toAttend(){
        return "attend";
    }

    /**
      @ author 一代天骄
      @ 2017/10/21 10:28
      @ Description 测试添加attend打卡
      */
    @RequestMapping("/sign")
    @ResponseBody
    public String signAttend(@RequestBody Attend attend) throws Exception {
        attendService.insertAttend(attend);
        return "user";
    }
    /**
      @ author 一代天骄
      @ 2017/10/21 12:57
      @ Description 考勤数据分页查询
      */
    @RequestMapping("/attendList")
    @ResponseBody
    public PageQueryBean listAttend(QueryCondition condition, HttpSession session){
        User user=(User) session.getAttribute("userInfo");
        String[] rangeDate=condition.getRangeDate().split("/");
        condition.setStartDate(rangeDate[0]);
        condition.setEndDate(rangeDate[1]);
        condition.setUserId(user.getId());
        PageQueryBean result=attendService.listAttend(condition);
        return result;
    }
}
