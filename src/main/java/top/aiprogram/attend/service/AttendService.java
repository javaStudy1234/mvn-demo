package top.aiprogram.attend.service;

import top.aiprogram.attend.entity.Attend;
import top.aiprogram.attend.vo.QueryCondition;
import top.aiprogram.common.page.PageQueryBean;

public interface AttendService {

    void  insertAttend(Attend attend) throws Exception;

    PageQueryBean listAttend(QueryCondition condition);
}
