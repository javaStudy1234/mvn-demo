package top.aiprogram.attend.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.aiprogram.attend.dao.AttendMapper;
import top.aiprogram.attend.entity.Attend;
import top.aiprogram.attend.service.AttendService;
import top.aiprogram.attend.vo.QueryCondition;
import top.aiprogram.common.page.PageQueryBean;
import top.aiprogram.common.utils.DateUtils;

import java.util.Date;
import java.util.List;

@Service("attendServiceImpl")
public class AttendServiceImpl implements AttendService {

    @Autowired
    private AttendMapper attendMapper;//测试插入的时候，这里没有Autowired，fuck
    /**
     * 中午12点，一判定早晚
     */
    private static final int NOON_HOUR=12;
    private static final int NOON_MINUTE=00;

    private Log log= LogFactory.getLog(AttendServiceImpl.class);

    @Override
    public void insertAttend(Attend attend) throws Exception {
        try {
            Date today=new Date();
            attend.setAttendDate(today);
            attend.setAttendWeek((byte)DateUtils.getTodayWeak());
            //查询当天，有没有此人打卡记录
            Attend todayRecord=attendMapper.selectTodayRecord(attend.getUserId());

            Date noon= DateUtils.getDate(NOON_HOUR,NOON_MINUTE);
            if (todayRecord==null){
                if (today.compareTo(noon)<=0){
                    attend.setAttendMorning(today);//早上打卡啊
                }else {
                    attend.setAttendEvening(today);//傍晚打卡
                }
                attendMapper.insertSelective(attend);
            }else {
                if (today.compareTo(noon)<=0){
                    return;
                }else {
                    todayRecord.setAttendEvening(today);//傍晚打卡
                    attendMapper.updateByPrimaryKeySelective(todayRecord);
                }
            }
        } catch (Exception e){
            log.error("用户签到异常",e);
            throw e;
        }
    }

    @Override
    public PageQueryBean listAttend(QueryCondition condition) {
        //根据条件，查询记录，如果有记录才会查询分页数据
        int count=attendMapper.countByCondition(condition);
        PageQueryBean pageResult=new PageQueryBean();
        if (count>0){
            pageResult.setTotalRows(count);
            pageResult.setCurrentPage(condition.getCurrentPage());
            pageResult.setPageSize(condition.getPageSize());
            List<Attend> attendList=attendMapper.selectAttendPage(condition);
            pageResult.setItems(attendList);
        }
        return pageResult;
    }
}
