package top.aiprogram.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    private SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private static Calendar calendar=Calendar.getInstance();

    /**
      @ author 一代天骄
      @ 2017/10/21 10:58
      @ Description 得到今天是周几
      */
    public static int getTodayWeak(){
        calendar.setTime(new Date());
        int weak=calendar.get(calendar.DAY_OF_WEEK)-1;
        //国外把0当成星期天了
        if (weak<0){
            weak=7;
        }
        return weak;
    }
    /**
      @ author 一代天骄
      @ 2017/10/21 10:58
      @ Description 计算时间差， 分钟数
      */
    public static int getMinute(Date startDate,Date endDate){
        long start=startDate.getTime();
        long end=endDate.getTime();
        int minute=(int)(end-start);
        return minute;
    }
    /**
      @ author 一代天骄
      @ 2017/10/21 11:07
      @ Description 获取当天的某刻时间
      */
    public static Date getDate(int hour,int minute){
        calendar.set(Calendar.HOUR_OF_DAY,hour);
        calendar.set(Calendar.MINUTE,minute);
        return calendar.getTime();
    }


    public static void main(String args[]){
        getTodayWeak();
    }
}
