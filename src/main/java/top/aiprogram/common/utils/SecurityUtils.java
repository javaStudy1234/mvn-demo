package top.aiprogram.common.utils;

import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SecurityUtils {
    /**
     * 加密密码
     * @param password
     * @return
     */
    public static String encrptyPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md5=MessageDigest.getInstance("MD5");
        //直接生成消息摘要能乱码的，用base64
        BASE64Encoder base64Encoder=new BASE64Encoder();
        String result=base64Encoder.encode(md5.digest(password.getBytes("UTF-8")));
        return result;
    }

    /**
     * 校验密码
     * @param inputPwd
     * @param dbPwd
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    public static boolean checkPassword(String inputPwd,String dbPwd) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String result=encrptyPassword(inputPwd);
        if (result.equals(dbPwd)){
            return true;
        }else {
            return false;
        }
    }

}
