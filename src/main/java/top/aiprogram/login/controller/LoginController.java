package top.aiprogram.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.aiprogram.common.utils.SecurityUtils;
import top.aiprogram.user.entity.User;
import top.aiprogram.user.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;
    /**
      @ author 一代天骄
      @ 2017/10/20 0:01
      @ Description 登陆界面
      */
    @RequestMapping
    public String login(){
        return "login";
    }

    /**
      @ author 一代天骄
      @ 2017/10/20 0:02
      @ Description 校验登陆
      */
    @RequestMapping("/check")
    @ResponseBody
    public String checkLogin(HttpServletRequest request) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = userService.findUserByUserName(username);
        if (user!=null){
            if (SecurityUtils.checkPassword(password,user.getPassword())){
                //设置session
                request.getSession().setAttribute("userInfo",user);
                return "login_succ";
            }else {
                return "login_fail";
            }
        }else {
            return "login_fail";
        }
    }
    /**
      @ author 一代天骄
      @ 2017/10/20 0:53
      @ Description 添加测试用户
      */
    @RequestMapping("/test")
    @ResponseBody
    public String createUser(@RequestBody User user) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        userService.createUser(user);
        return "succ";
    }
}
