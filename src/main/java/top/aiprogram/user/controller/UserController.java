package top.aiprogram.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.aiprogram.user.entity.User;
import top.aiprogram.user.service.UserService;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/index")
    public String user(){
        User user=new User();
        user.setUsername("令狐老祖");
        user.setPassword("123456");
        user.setMobile("18162775609");
        user.setRealName("菜鸟");
    //  userService.createUser(user);
        return "user";
    }
    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/home")
    public String home(){
        return "home";
    }
    /**
      @ author 一代天骄
      @ 2017/10/21 0:31
      @ Description 获取用户信息的
      */
    @RequestMapping("/userInfo")
    @ResponseBody
    public User getUser(HttpSession session){
        User user=(User) session.getAttribute("userInfo");
        return user;
    }
    /**
      @ author 一代天骄
      @ 2017/10/21 0:42
      @ Description 退出系统
      */
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.invalidate();
        return "login";
    }
}
