package top.aiprogram.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.aiprogram.common.utils.SecurityUtils;
import top.aiprogram.user.dao.UserMapper;
import top.aiprogram.user.entity.User;
import top.aiprogram.user.service.UserService;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    @Transactional
    public void createUser(User user) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        user.setPassword(SecurityUtils.encrptyPassword(user.getPassword()));
        userMapper.insertSelective(user);
    }
    /**
      @ author 一代天骄
      @ 2017/10/20 0:03
      @ Description 根据用户名查询用户
      */
    @Override
    public User findUserByUserName(String username) {
        User user = userMapper.selectByUserName(username);
        return user;
    }
}
